//
//  MainViewController.swift
//  SampleApp
//
//  Created by Mohamed Hajlaoui on 09/03/2020.
//  Copyright © 2020 Mohamed Hajlaoui. All rights reserved.
//

import UIKit
import WebKit

class MainViewController: UIViewController {
    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let barButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(reloadWebView))
        barButton.accessibilityLabel = "refresh_button"
        self.navigationItem.rightBarButtonItem = barButton
        
        self.loadWebView()
    }

    func loadWebView() {
        guard let url = URL(string: "https://datadome.co/") else {
            return
        }
        
        let request = URLRequest(url: url)
        self.webView.load(request)
    }
    
    @objc
    func reloadWebView() {
        self.webView.reload()
    }
    
    
}
