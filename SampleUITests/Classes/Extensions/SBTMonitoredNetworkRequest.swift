//
//  SBTMonitoredNetworkRequest.swift
//  SampleUITests
//
//  Created by Med Hajlaoui on 11/03/2020.
//  Copyright © 2020 Mohamed Hajlaoui. All rights reserved.
//
import Foundation
import SBTUITestTunnelClient

internal extension SBTMonitoredNetworkRequest {
    var identifier: String {
        let url = "\(self.request?.url?.host ?? "")\(self.request?.url?.relativePath ?? "")"

        if url == TestCaseConfigurationRequest.reportRequest.baseURL {
            return TestCaseConfigurationRequest.reportRequest.id
        }
        
        return url
    }
    
    var dictionary: [String: Any] {
        var dic = [String: Any]()
        
        // http method
        let httpMethod = self.request?.httpMethod ?? ""
        dic["httpMethod"] = httpMethod

        // query params
        let parameters: [String: Any] = self.request?.url?.queryParameters ?? [:]
        dic["parameters"] = parameters

        // body
        var body = [[String: Any]]()
        dic["body"] = body

        guard let requestBodyData  = self.request?.httpBody else {
            return dic
        }
        
        guard let requestBody = try? JSONSerialization.jsonObject(with: requestBodyData, options: .allowFragments) as? [[String: Any]] else {
            return dic
        }
        
        body = requestBody
        dic["body"] = body
        return dic
    }
    
}
