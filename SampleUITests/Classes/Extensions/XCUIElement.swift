//
//  XCUIElement.swift
//  SampleUITests
//
//  Created by Med Hajlaoui on 11/03/2020.
//  Copyright © 2020 Mohamed Hajlaoui. All rights reserved.
//

import Foundation
import XCTest

extension XCUIElement {
    func safeTap() {
        if self.exists && self.isHittable {
            self.tap()
        }
    }
}
