//
//  Bundle.swift
//  SampleUITests
//
//  Created by Med Hajlaoui on 11/03/2020.
//  Copyright © 2020 Mohamed Hajlaoui. All rights reserved.
//

import Foundation

extension Bundle {
    static var testBundle: Bundle? {
        let bundle = Bundle.allBundles.first(where: { $0.bundlePath.hasSuffix(".xctest") })
        return bundle
    }
    
    static func stubData(fileName: String) -> Data? {
        guard let path = Bundle.testBundle?.path(forResource: fileName, ofType: "json") else {
            return nil
        }
    
        let data = try? Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
        return data
    }
}
