//
//  SampleTestClass.swift
//  SampleIntegrationUITests
//
//  Created by Med Hajlaoui on 10/03/2020.
//  Copyright © 2020 Mohamed Hajlaoui. All rights reserved.
//

import SBTUITestTunnelClient

@testable import SampleSDK

struct ReportLogsTestCaseConfigutation: TestCaseConfigurationProtocol {
    var stubs: [TestCaseConfigurationStub] {
        let stubs = self.defaultStubs()
        return stubs
    }
    
    var expectedRequests: [TestCaseConfigurationRequest] {
        let requests = self.defaultExpectedRequests()
        return requests
    }
    
    static var placeholders: [String: String] {
        let sdkVersion: String = Bundle(for: SampleSDK.self).infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        let appVersion: String = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        let appBuildVersion: String = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
        
        let sysDate: String = {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            return formatter.string(from: Date())
        }()
        
        let timezone: String = TimeZone.current.localizedName(for: NSTimeZone.NameStyle.standard, locale: Locale.current) ?? ""
        let language: String = NSLocale.preferredLanguages.first ?? ""
        let country: String = Locale.current.regionCode ?? ""
        
        let deviceModel: String = {
            var systemInfo = utsname()
            uname(&systemInfo)
            
            let data = Data(bytes: &systemInfo.machine, count: Int(_SYS_NAMELEN))
            guard let descrition = String(bytes: data, encoding: .ascii) else {
                return ""
            }
            
            return descrition.trimmingCharacters(in: .controlCharacters)
        }()
        
        let osVersion: String = UIDevice.current.systemVersion
        
        return [
            "app_build_version": appBuildVersion,
            "app_version": appVersion,
            "sdk_version": sdkVersion,
            "language": language,
            "system_date": sysDate,
            "os_version": osVersion,
            "time_zone": timezone,
            "country": country,
            "device_model": deviceModel
        ]
    }
}


class SampleTestClass: TestCase {
    override func setUp() {
        self.configuration = ReportLogsTestCaseConfigutation()
        self.responseTime = NetworkingSpeed.speedWifi.rawValue

        super.setUp()
    }
    
    func testReportLogs() {
        launchTest(orientation: .portrait)
    }
}
