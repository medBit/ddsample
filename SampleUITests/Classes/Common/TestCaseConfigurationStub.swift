//
//  TestCaseConfigurationStub.swift
//  SampleUITests
//
//  Created by Mohamed Hajlaoui on 11/03/2020.
//  Copyright © 2020 Mohamed Hajlaoui. All rights reserved.
//

import Foundation

enum TestCaseConfigurationStubType {
    case report
    
    var urlRegExp: String {
        switch self {
        case .report:
            return "https://google.com/report"
        }
    }

    var name: String {
        switch self {
        case .report:
            return "report"
        }
    }
}

//swiftlint:disable explicit_enum_raw_value
enum TestCaseConfigurationStubResponseType: String {
    case success
    case fail
}
//swiftlint:enable explicit_enum_raw_value


struct TestCaseConfigurationStub {
    init(type: TestCaseConfigurationStubType,
         responseType: TestCaseConfigurationStubResponseType? = nil,
         errorCode: Int? = nil,
         returnCode: Int = 200) {
        
        self.type = type
        self.responseType = responseType
        self.errorCode = errorCode
        self.returnCode = returnCode
    }
    
    /// The type of the endpoint
    var type: TestCaseConfigurationStubType
    
    /// The type of the reponse to stub, if any
    var responseType: TestCaseConfigurationStubResponseType?
    
    /// The HTTP error if any
    var errorCode: Int?

    /// The HTTP return code if any
    var returnCode: Int

    /// The data containted to stub
    var responseData: Data? {
        var responseTypeString = ""
        if let responseType = self.responseType {
            responseTypeString = "-\(responseType.rawValue)"
        }
        let fileName = "stub-\(self.type.name)\(responseTypeString)"
        
        guard let url = Bundle.testBundle?.url(forResource: fileName, withExtension: "json") else {
            return nil
        }
        
        
        do {
            var string = try String(contentsOf: url)
            
            let placeholders: [String: String]
            switch self.type {
            case .report:
                placeholders = ReportLogsTestCaseConfigutation.placeholders
            /*default:
                placeholders = [:]*/
            }
            
            string.replacePlaceholders(placeholders)
            
            return string.data(using: .utf8)
        } catch {
            print("Error reading stub file \(error.localizedDescription)")
        }
        
        return nil
    }
}

extension String {
    mutating func replacePlaceholders(_ placeholders: [String: String] = [:]) {
        for placeholder in placeholders {
            var key = placeholder.key
            if !key.hasPrefix("{") {
                key = "{\(key)"
            }
            if !key.hasSuffix("}") {
                key = "\(key)}"
            }

            self = self.replacingOccurrences(of: key, with: placeholder.value)
        }        
    }

}
