//
//  TestCase.swift
//  SampleUITests
//
//  Created by Mohamed Hajlaoui on 11/03/2020.
//  Copyright © 2020 Mohamed Hajlaoui. All rights reserved.
//

import XCTest
import SBTUITestTunnelClient
import Nimble

class TestCase: XCTestCase {

    // MARK: - Properties
    var configuration: TestCaseConfigurationProtocol?
    var responseTime: Double?
   
    // MARK: - Lifecycle
    override func setUp() {
        super.setUp()
        
        continueAfterFailure = true
        
        app.launchTunnel(withOptions: [SBTUITunneledApplicationLaunchOptionResetFilesystem]) { [unowned self] in
            self.configuration?.stubs.forEach { endpoint in
                if let errorCode = endpoint.errorCode {
                    let match = SBTRequestMatch(url: endpoint.type.urlRegExp)
                    let response = SBTStubFailureResponse(errorCode: errorCode)
                    self.app.stubRequests(matching: match,
                                          response: response)
                } else if let responseData = endpoint.responseData {
                    let match = SBTRequestMatch(url: endpoint.type.urlRegExp)
                    let response = SBTStubResponse(response: responseData, returnCode: endpoint.returnCode)
                    self.app.stubRequests(matching: match,
                                          response: response)
                }
            }
            
            self.configuration?.monitoredURLs.forEach { url in
                self.app.monitorRequests(matching: SBTRequestMatch(url: url))
                if let responseTime = self.responseTime {
                    self.app.throttleRequests(matching: SBTRequestMatch(url: url), responseTime: responseTime)
                }
            }
        }
    }
    
    override func tearDown() {
        super.tearDown()
        
        app.stubRequestsRemoveAll()
        app.throttleRequestRemoveAll()
        app.monitorRequestRemoveAll()
    }
    
    // MARK: - Tests Methods
    func launchTest(orientation: UIDeviceOrientation) {
        if self.configuration == nil {
            XCTFail("The test configuration was not defined")
            return
        }

        XCUIDevice.shared.orientation = orientation
        
        startTesting()
        
        tapButon("refresh_button")
    
        finishTesting()
    }
    
    // MARK: - UI Utils
    func tapButon(_ buttonIdentifier: String) {
        app.buttons[buttonIdentifier].safeTap()
    }
    
    // MARK: - Test Utils
    func startTesting() {
        if self.configuration  == nil {
            XCTFail("The test configuration was not defined")
            return
        }
        
        sleep(2)
        takeScreenshot(name: "start")
    }
    
    func finishTesting() {
        if self.configuration  == nil {
            XCTFail("The test configuration was not defined")
            return
        }

        sleep(2)
        takeScreenshot(name: "end")

        assertRequests()
    }
    
    // MARK: - Assertion Utils
    func assertRequests() {
        guard let configuration = self.configuration else {
            XCTFail("The test configuration was not defined")
            return
        }

        let monitoredRequests: [SBTMonitoredNetworkRequest] = app.monitoredRequestsFlushAll()
        var monitoredRequestsDic = [String: Any]()
        
        var duplicatedRequests = [String]()
        
        for request in monitoredRequests {
            let id = request.identifier
            
            if monitoredRequestsDic[id] != nil {
                duplicatedRequests.append(id)
            }
            
            monitoredRequestsDic[id] =  request.dictionary
        }
        
        let expectedRequests = configuration.expectedRequests
        var expectedRequestsDic = [String: Any]()
        for request in expectedRequests {
            let id = request.id
            expectedRequestsDic[id] = request.dictionary
        }
        
        self.assert(dic: monitoredRequestsDic, equalTo: expectedRequestsDic)
        
        if !configuration.allowDuplicatedRequests {
            Swift.assert(duplicatedRequests.isEmpty, "The following requests are sent more than once: \n \(duplicatedRequests)")
        }
    }
    
    func assert(dic dictionary: [String: Any], equalTo expectedDictionary: [String: Any]) {
        for key in expectedDictionary.keys {
            if !dictionary.keys.contains(key) {
                XCTFail("\(dictionary.keys) does not have the key \(key) as expected")
            }
        }
        
        for key in dictionary.keys {
            if !expectedDictionary.keys.contains(key) {
                XCTFail("\(expectedDictionary.keys) has an unexpected key \(key)")
            }
            
            let value = dictionary[key]
            let expectedValue = expectedDictionary[key]
            self.assert(obj: value, equalTo: expectedValue, forKey: key)
        }
    }
    
    func assert(obj obj1: Any?, equalTo obj2: Any?, forKey key: String) {
        if (obj1 != nil && obj2 == nil)
            || (obj1 == nil && obj2 != nil) {
            XCTFail("\(String(describing: obj1)) \nand \n\(String(describing: obj2))\n do not match")
            return
        }
        
        guard let value = obj1, let expected = obj2 else {
            return
        }
        
        if let stringValue = value as? String, let stringExpected = expected as? String {
            self.assert(string: stringValue, equalTo: stringExpected, forKey: key)
            return
        }
        
        if let arrayValue = value as? [Any?], let arrayExpected = expected as? [Any?] {
            self.assert(array: arrayValue, equalTo: arrayExpected)
            return
        }
        
        if let dicValue = value as? [String: Any], let dicExpected = expected as? [String: Any] {
            self.assert(dic: dicValue, equalTo: dicExpected)
        }
    }
    
    func assert(string string1: String, equalTo string2: String, forKey key: String) {
        //ignore "{ignore}" value
        if string2 == "{ignore}" {
            return
        }
        
        if string1 != string2 {
            XCTFail("Expected value \(string2) for key \(key). Found \(string1)")
            return
        }
        
        print("Validated '\(string1)' is equal to '\(string2)' for key \(key)")
        return
    }
    
    func assert(array array1: [Any?], equalTo array2: [Any?]) {
        if array1.count != array2.count {
            XCTFail("\(array1) \nand \n\(array2) \ndo not have the same count of element")
            return
        }
        
        for i in 0...array1.count-1 {
            self.assert(obj: array1[i], equalTo: array2[i], forKey: "Array element")
        }
        return
    }
    
    func takeScreenshot(name: String) {
        guard self.configuration?.takeScreenshots ?? false else {
            return
        }
        
        let orientation = XCUIDevice.shared.orientation.isPortrait ? "portrait" : "landscape"
        
        let screenshot = app.windows.firstMatch.screenshot()
        let attachment = XCTAttachment(screenshot: screenshot)
        attachment.lifetime = .keepAlways
        
        let className = NSStringFromClass(type(of: self)).components(separatedBy: ".").last ?? ""
        let fileName = ("\(className)_\(name)_\(orientation)").replacingOccurrences(of: "_", with: "-")
        attachment.name = fileName
        add(attachment)
    }
}
