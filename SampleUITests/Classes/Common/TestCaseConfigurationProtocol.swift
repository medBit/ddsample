//
//  TestCaseConfigurationProtocol.swift
//  SampleUITests
//
//  Created by Mohamed Hajlaoui on 11/03/2020.
//  Copyright © 2020 Mohamed Hajlaoui. All rights reserved.
//

import Foundation
import XCTest

protocol TestCaseConfigurationProtocol {
    
    /// Indicates if the test should take screenshots
    var takeScreenshots: Bool { get }

    /// Indicates if the duplicated requests are allowed
    var allowDuplicatedRequests: Bool { get }

    /// The stubs to use during the test
    var stubs: [TestCaseConfigurationStub] { get }
    
    /// The expected requests to be sent during th test
    var expectedRequests: [TestCaseConfigurationRequest] { get }
    
    /// The URLs to monitor
    var monitoredURLs: [String] { get }

    /// Initializer used to instantiate the configuration object
    init()
}

extension TestCaseConfigurationProtocol {
    /// Default implementation of monitoredURLs var
    var monitoredURLs: [String] {
        let urls = self.defaultMonitoredURLs
        return urls
    }
    
    /// Default implementation of takeScreenshots var
    var takeScreenshots: Bool {
        let flag = true
        return flag
    }
    
    var allowDuplicatedRequests: Bool {
        let flag = true
        return flag
    }
    
    var expectedRequests: [TestCaseConfigurationRequest] {
        let requests = self.expectedRequests
        return requests
    }
    
}

extension TestCaseConfigurationProtocol {
    func defaultStubs() -> [TestCaseConfigurationStub] {
        
        let stubs: [TestCaseConfigurationStub?] = [
            TestCaseConfigurationStub(type: .report, responseType: .success),
            TestCaseConfigurationStub(type: .report, responseType: .fail)
        ]
        
        let tunnel = stubs.compactMap { $0 }
        return tunnel
    }
    
    var defaultMonitoredURLs: [String] {
        let urls = ["google.com"]
        return urls
    }
    
    
    func defaultExpectedRequests() -> [TestCaseConfigurationRequest] {
        let requests: [TestCaseConfigurationRequest] = [.reportRequest]
        return requests
    }
}
