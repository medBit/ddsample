//
//  NetworkingSpeed.swift
//  SampleUITests
//
//  Created by Mohamed Hajlaoui on 11/03/2020.
//  Copyright © 2020 Mohamed Hajlaoui. All rights reserved.
//

import UIKit

enum NetworkingSpeed: Double {
    case speedGPRS = -0.125 // -1 / 8 kbps
    case speedEDGE = -16.0 // -128 / 8 kbps
    case speed3G = -400.0  // -3200/ 8 kbps
    case speed3GPlus = -900.0  // -7200 / 8 kbps
    case speedWifi = -1500.0 //-12000 / 8 kbps
}
