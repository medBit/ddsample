//
//  TestCaseConfigurationRequest.swift
///  SampleUITests
//
//  Created by Mohamed Hajlaoui on 11/03/2020.
//  Copyright © 2020 Mohamed Hajlaoui. All rights reserved.
//

import Foundation

enum TrackerEvent: String {
    case reportEvent = "report"
}

enum TestCaseConfigurationRequest {
    case reportRequest
    
    /// The URL of the request (without the parameters)
    var baseURL: String {
        switch self {
        case .reportRequest:
            return "google.com/report"
        }
    }
    
    var id: String {
        switch self {
        case .reportRequest:
            return self.baseURL
        }
    }

    /// The expected method of the request
    var httpMethod: String {
        switch self {
        case .reportRequest:
            return "POST"
        }
    }

    /// A dictionary representing the expected body of the request
    var body: [String: Any] {
        let body = deserializeFile(for: "request-body")
        return body
    }
    
    /// A dictionary representing the expected parameters
    var parameters: [String: Any] {
        let params = deserializeFile(for: "request-parameters")
        return params
    }

    // MARK: - Utils
    var name: String {
        switch self {
        case .reportRequest:
            return "report"
        }
    }
    
    private func deserializeFile(for prefix: String) -> [String: Any] {
        var dictionary: [String: Any] = [:]
        let fileName = "\(prefix)-\(self.name)"
        
        guard let url = Bundle.testBundle?.url(forResource: fileName, withExtension: "json") else {
            return [:]
        }
        
        
        do {
            var string = try String(contentsOf: url)
            var placeholders: [String: String] = [:]
            switch self {
            case .reportRequest:
                placeholders = ReportLogsTestCaseConfigutation.placeholders
            }

            string.replacePlaceholders(placeholders)
            if let data = string.data(using: .utf8) {
                dictionary = try (JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] ?? [:])
            }
        } catch {
            print("Serialization error \(error.localizedDescription)")
        }

        return dictionary
    }
}

extension TestCaseConfigurationRequest {
    var dictionary: [String: Any] {
        var dic = [String: Any]()
        
        let httpMethod = self.httpMethod
        let body = self.body
        let parameters = self.parameters
        
        dic["httpMethod"] = httpMethod
        dic["body"] = body["result"]
        dic["parameters"] = parameters
        return dic
    }
}
