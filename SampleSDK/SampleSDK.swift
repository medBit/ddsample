//
//  SampleSDK.swift
//  SampleSDK
//
//  Created by Mohamed Hajlaoui on 09/03/2020.
//  Copyright © 2020 Mohamed Hajlaoui. All rights reserved.
//

import UIKit

public class SampleSDK: NSObject {
    public static func setLogLevel(level: LogLevel) {
        Logger.logLevel = level
    }
    
    @objc
    public static func start() {
        if let sdkVersion = Bundle(for: SampleSDK.self).infoDictionary?["CFBundleShortVersionString"] {
            print("[SampleSDK] Version \(sdkVersion)")
        }
        
        //check api key
        if Bundle.main.object(forInfoDictionaryKey: "SampleAPIKey") == nil {
            print("[SampleSDK] Missing SampleAPIKey in your Info.plist")
            return
        }
        
        //log device
        AnalyticsManager.shared.log(event: SessionEvent())
    }
}
