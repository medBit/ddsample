//
//  Cachable.swift
//  SampleSDK
//
//  Created by Mohamed Hajlaoui on 10/03/2020.
//  Copyright © 2020 Mohamed Hajlaoui. All rights reserved.
//

import UIKit

protocol Cachable: Codable {
    
    /// A key of the local storage
    static var localStorageKey: String { get }
    
    /// A unique identifier to be used for saving the object locally.
    var key: String { get }

    /// Persists the bound object / list to llocal storage.
    func cache() throws
    
    /// Unpersist the bound object / list from the local storage
    func uncache() throws
}
