//
//  AnalyticsManager.swift
//  SampleSDK
//
//  Created by Mohamed Hajlaoui on 09/03/2020.
//  Copyright © 2020 Mohamed Hajlaoui. All rights reserved.
//

import Foundation

internal class AnalyticsManager {
    static let shared = AnalyticsManager()
    private var scheduler: Scheduler
    
    init() {
        self.scheduler = Scheduler()
        self.scheduler.action = { [weak self] in
            self?.processLogs()
        }
        self.scheduler.start()
    }
    
    /// Logs an event passed as parameter
    /// - Parameter event: The device event to log
    func log(event: SessionEvent) {
        Logger.info("Logging device \(event)")
        
        //cache the event for later processing
        try? event.cache()
    }
    
    /// Send locally cached logs to the server
    func processLogs() {
        let logs = SessionEvent.loadLogs(withPageSize: 10)
        if logs.isEmpty {
            self.scheduler.actionDidFail()
            return
        }
        
        Logger.info("Reporting \(logs.count) events")
        
        APIManager.report(logs: logs) { [weak self] result in
            switch result {
            case .success(let reportedEvents):
                // when succeed, reported logs got removed from local cache
                try? reportedEvents.result.uncache()
                self?.scheduler.actionDidSucceed()
            case .failure(let error):
                Logger.error("Failed to report cached logs \(error.localizedDescription)")
                self?.scheduler.actionDidFail()
            }
        }
    }
}
