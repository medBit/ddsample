//
//  Scheduler.swift
//  SampleSDK
//
//  Created by Med Hajlaoui on 10/03/2020.
//  Copyright © 2020 Mohamed Hajlaoui. All rights reserved.
//

import UIKit

typealias ExecutionBlock = (() -> Void)

/// A convinient helper to schedule batching events to server
internal class Scheduler {
    
    /// An action to fire each time the scheduler timer is due
    var action: ExecutionBlock?
    
    /// The time interval after which the action is cheduled
    private var timeInterval: TimeInterval = 1.0
    
    /// The max time to wait in case of exponential backoff. Max 15 mins interval
    private let maxTimeInterval: TimeInterval = 900.0
    
    /// Start the scheduler and fire the first timer
    func start() {
        scheduleAction()
    }
    
    /// The time interval is reset to the initial value
    func actionDidSucceed() {
        timeInterval = 1.0
        scheduleAction()
    }
    
    /// The time interval is increased in an exponential fashion
    func actionDidFail() {
        timeInterval = min(timeInterval * 2, maxTimeInterval)
        scheduleAction()
    }
    
    /// Schedule the action
    func scheduleAction() {
        DispatchQueue.main.asyncAfter(deadline: .now() + timeInterval) { [weak self] in
            self?.action?()
        }
    }
}
