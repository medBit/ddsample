//
//  NSObject+DDLoader.m
//  SampleSDK
//
//  Created by Mohamed Hajlaoui on 09/03/2020.
//  Copyright © 2020 Mohamed Hajlaoui. All rights reserved.
//

#import "NSObject+DDLoader.h"
#import <UIKit/UIKit.h>
#import <SampleSDK/SampleSDK-Swift.h>


@implementation NSObject (DDLoader)

/// Overriding this method to auto fire the SDK at app launch
+ (void)load {
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
           
   [center addObserver:self
              selector:@selector(dd_applicationDidBecomeActive)
                  name:UIApplicationDidBecomeActiveNotification
                object:nil];
}

#pragma mark - Application Lifecycle
- (void)dd_applicationDidBecomeActive {
    [SampleSDK start];
}
@end

