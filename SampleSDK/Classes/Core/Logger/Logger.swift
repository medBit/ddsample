//
//  Logger.swift
//  SampleSDK
//
//  Created by Mohamed Hajlaoui on 09/03/2020.
//  Copyright © 2020 Mohamed Hajlaoui. All rights reserved.
//

import Foundation

public enum LogLevel {
    case none
    case info
    case warning
    case error
    case verbose
}

internal enum Logger {
    internal static var logLevel: LogLevel = .verbose
    
    /// Logs a message with the info level
    /// - Parameter message: The message to log
    internal static func info(_ message: String) {
        if Logger.logLevel != .info &&
            Logger.logLevel != .verbose {
            return
        }
        
        print("\n[SampleSDK][INFO] \(message)")
    }
    
    /// Logs a message with the warning level
    /// - Parameter message: The message to log
    internal static func warn(_ message: String) {
        if Logger.logLevel == .none ||
            Logger.logLevel == .error {
            return
        }
        
        print("\n[SampleSDK][WARN] \(message)")
    }
    
    /// Logs a message with the error level
    /// - Parameter message: The message to log
    internal static func error(_ message: String) {
        if Logger.logLevel == .none {
                return
        }
        
        print("\n[SampleSDK][ERRO] \(message)")
    }
}
