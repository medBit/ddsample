//
//  ReportResponse.swift
//  SampleSDK
//
//  Created by Mohamed Hajlaoui on 11/03/2020.
//  Copyright © 2020 Mohamed Hajlaoui. All rights reserved.
//

import UIKit

/// A model of an api response
class ReportResponse: Codable {
    var status: String
    var result: [SessionEvent]
}
