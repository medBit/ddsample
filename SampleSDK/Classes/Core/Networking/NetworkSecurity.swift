//
//  NetworkSecurity.swift
//  SampleSDK
//
//  Created by Med Hajlaoui on 11/03/2020.
//  Copyright © 2020 Mohamed Hajlaoui. All rights reserved.
//

import Foundation


/// A helper to load the certificate used for pinning
internal enum NetworkSecurity {
  internal static let certificate = certificateData(fromFileNamed: "google.com")
    
    /// Loads data from the certificate file
    /// - Parameter filename: The name of the certificate file
  private static func certificateData(fromFileNamed filename: String) -> Data {
    guard let filePath = Bundle(for: SampleSDK.self).path(forResource: filename, ofType: "der") else {
        fatalError("Certificate file not found")
    }
    
    guard let data = try? Data(contentsOf: URL(fileURLWithPath: filePath)) else {
        fatalError("Corrupt Certificate file")
    }
    
    return data
  }
}
