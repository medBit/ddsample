//
//  APIManager.swift
//  SampleSDK
//
//  Created by Med Hajlaoui on 10/03/2020.
//  Copyright © 2020 Mohamed Hajlaoui. All rights reserved.
//

import UIKit
import Security

/// A helper class to handle api calls
internal class APIManager: NSObject, URLSessionDelegate {
    private static let logsEndpointUrl: String = "https://google.com/report"
    
    /// A high level representation of API errors
    internal enum APIError: Error {
        case apiError(_ error: Error)
        case invalidResponse
        case invalidURL
        case noData
        case invalideCertificate
        case decodingError
        case encodingError
    }
    
    
    /// Performs a POST request to report a batch of events to the api
    /// - Parameters:
    ///   - logs: The list of events to report
    ///   - completion: The completion handler
    static func report(logs: [SessionEvent], completion: @escaping (Result<ReportResponse, APIError>) -> Void) {
        // create the url
        guard let url = URL(string: logsEndpointUrl) else {
            completion(.failure(.invalidURL))
            return
        }
        
        // create the post request
        var request = URLRequest(url: url)
        request.httpMethod = "POST"

        // create the json body
        let encoder = JSONEncoder()
        guard let encodedData = try? encoder.encode(logs) else {
            completion(.failure(.encodingError))
            return
        }
        request.httpBody = encodedData

        // fire the request
        let session = URLSession(
            configuration: .ephemeral,
            delegate: URLSessionPinningDelegate(),
            delegateQueue: nil
        )
        
        session.dataTask(with: request) { data, _, error in
            if let error = error {
                completion(.failure(.apiError(error)))
                return
            }
            
            guard let data = data else {
                completion(.failure(.noData))
                return
            }
            
            let decoder = JSONDecoder()
            guard let result = try? decoder.decode(ReportResponse.self, from: data) else {
                completion(.failure(.decodingError))
                return
            }

            completion(.success(result))
        }.resume()
    }
}
