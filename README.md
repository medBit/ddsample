
# DDSample

## Getting Started
The project should be runnable at checkout from git. Use the following command at the project directory to install any missing dependencies.

```sh
pod install
```


## Workspace structure

The workspace hosts 3 mains modules

- `SampleApp`: a sample app used as target app for the SampleUITests module and embedding the SampleSDK module.
- `SampleSDK`: a dynamic framework hosting the code for reporting the state of the device to a REST api. The framework runs automatically at the app launch, no need to import statements or initialization code.
- `SampleUITests`: a UI tests module used to mock the api side. This module will launch the target app (SampleApp) which will run the SampleSDK automatically. Any call to the network made by the SDK is monitored and its body and response are validated / rewritten.

### SampleApp module

The app contains a single view where a web view is loading the datadome website. 

### SampleSDK module

This dynamic framework runs each time the app become active. It captures the current state of the device, caches it in the local storage. A cron-like job is running periodically (with an exponential backoff mechanism in case of failure) and sending batches of cached events to the api. Once acknowledged, the reported logs are removed from local storage.

The following is the list of the main classes and their usage:

- `Loader/NSObject+DDLoader` Used to attach an observer on the appDidBecomeActive event. 
Once fired, the SDK will log the current state of the device.
- `Core/Networking` This package hosts main classes for Networking management (security, api calls... etc)
	-  `Networking/APIManager` A wrapper around URLSession providing a convinient way to perform REST api calls and receive a Result with success of failure.
	-  `Networking/URLSessionPinningDelegate` A class managing challenges resolving and validating the server identity (Certificate Pinning implementation)
-  `Core/Logger` A light logger for convinient logging
-  `Analytics/SessionEvent` The main model. Representing the device state at the moment of its instanciation. The model handle data structure and caching / uncaching
-  `Analytics/AnalyticsManager` The main component of the SDK. This central manager caches events and uses a scheduler to batch send cached events to the api.
-  `Analytics/Scheduler` Used to schedule attempts of sending batches of events to the api.

### SampleUITests module

This module is used to mock api calls. The SDK is tested in black box mode. All network calls are monitored, and all requests are validated according to an expected behavior.